package com.mobile.clailton.jogodamemoria;

import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.mobile.clailton.jogodamemoria.adapter.GridViewAdapter;
import com.mobile.clailton.jogodamemoria.model.ItemMemoria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    GridView gridView;
    GridViewAdapter adapter;
    Button botaoIniciar;
    ArrayList<ItemMemoria> listaItens = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        gridView = (GridView)findViewById(R.id.gridView);
        botaoIniciar = (Button)findViewById(R.id.buttonIniciar);
        adapter = new GridViewAdapter(this, listaItens, false);

        gridView.setAdapter(adapter);

        botaoIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DelayActivity.class);
                intent.putExtra("listaItens", listaItens);
                startActivity(intent);
            }
        });

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Collections.shuffle(listaItens);
    }


    public void init() {
        TypedArray imgs = this.getResources().obtainTypedArray(R.array.lista_imagens);

        for (int i = 0; i < imgs.length(); i++) {
            ItemMemoria itemMemoria1 = new ItemMemoria(imgs.getResourceId(i, 0));
            ItemMemoria itemMemoria2 = new ItemMemoria(imgs.getResourceId(i, 0));

            listaItens.add(itemMemoria1);
            listaItens.add(itemMemoria2);
        }

        Collections.shuffle(listaItens);
        imgs.recycle();
        imgs = null;
    }

}
