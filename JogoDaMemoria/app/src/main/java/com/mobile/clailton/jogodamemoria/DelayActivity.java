package com.mobile.clailton.jogodamemoria;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.mobile.clailton.jogodamemoria.adapter.GridViewAdapter;
import com.mobile.clailton.jogodamemoria.model.ItemMemoria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DelayActivity extends AppCompatActivity {

    GridView gridViewDelay;
    GridViewAdapter adapter;
    ItemMemoria primeiroItemEscolhido;
    ItemMemoria segundoItemEscolhido;
    ArrayList<ItemMemoria> listaItens = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delay);
        Intent intent = this.getIntent();
        if (intent.getSerializableExtra("listaItens") == null) {
            finish();
        }

        listaItens = (ArrayList)intent.getSerializableExtra("listaItens");

        gridViewDelay = (GridView)findViewById(R.id.gridViewDelay);
        adapter = new GridViewAdapter(this, listaItens, true);

        gridViewDelay.setAdapter(adapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intentProximaTela = new Intent(DelayActivity.this, JogoActivity.class);
                intentProximaTela.putExtra("listaItens", listaItens);
                startActivity(intentProximaTela);
                finish();
            }
        }, 1000);

    }


}
