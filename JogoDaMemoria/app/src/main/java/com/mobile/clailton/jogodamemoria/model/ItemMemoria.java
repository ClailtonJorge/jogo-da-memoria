package com.mobile.clailton.jogodamemoria.model;

import java.io.Serializable;

/**
 * Created by clail on 25/03/2016.
 */
public class ItemMemoria implements Serializable {

    private int imagem;


    public ItemMemoria() {
    }

    public ItemMemoria(int imagem) {
        this.imagem = imagem;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

}
