package com.mobile.clailton.jogodamemoria.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mobile.clailton.jogodamemoria.R;
import com.mobile.clailton.jogodamemoria.model.ItemMemoria;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by clail on 25/03/2016.
 */
public class GridViewAdapter extends BaseAdapter {

    List<ItemMemoria> listaItens = new ArrayList<>();
    Context context;
    boolean viradaFace;


    public GridViewAdapter(Context context, List<ItemMemoria> listaItens, boolean viradaFace) {
        this.context = context;
        this.listaItens = listaItens;
        this.viradaFace = viradaFace;
    }

    @Override
    public int getCount() {
        return listaItens.size();
    }

    @Override
    public ItemMemoria getItem(int position) {
        return listaItens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_gridview, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.imageItem = (ImageView)convertView.findViewById(R.id.imageView);
            viewHolder.imageItem2 = (ImageView)convertView.findViewById(R.id.imageView2);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        ItemMemoria itemMemoria = this.listaItens.get(position);


        viewHolder.imageItem.setImageResource(itemMemoria.getImagem());
        viewHolder.imageItem.setAdjustViewBounds(true);
        viewHolder.imageItem2.setAdjustViewBounds(true);

        if (viradaFace) {
            viewHolder.imageItem.setVisibility(View.VISIBLE);
            viewHolder.imageItem2.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }


    private static class ViewHolder {

        ImageView imageItem;
        ImageView imageItem2;

    }


}
