package com.mobile.clailton.jogodamemoria;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DigitalClock;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.clailton.jogodamemoria.adapter.GridViewAdapter;
import com.mobile.clailton.jogodamemoria.model.ItemMemoria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;

public class JogoActivity extends AppCompatActivity {


    GridView gridViewJogo;
    GridViewAdapter adapter;
    Button botaoDesistir;
    ItemMemoria primeiroItemEscolhido;
    ItemMemoria segundoItemEscolhido;
    ArrayList<ItemMemoria> listaItens = new ArrayList<>();

    List<ImageView> item1 = new ArrayList<>();
    List<ImageView> item2 = new ArrayList<>();
    int quantEscolhidos;
    int acertos = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jogo);

        Intent intent = this.getIntent();
        if (intent.getSerializableExtra("listaItens") == null) {
            finish();
        }

        listaItens = (ArrayList)intent.getSerializableExtra("listaItens");

        gridViewJogo = (GridView)findViewById(R.id.gridViewJogo);
        botaoDesistir = (Button)findViewById(R.id.buttonDesistir);

        adapter = new GridViewAdapter(this, listaItens, false);

        gridViewJogo.setAdapter(adapter);
        gridViewJogo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pegarItens(view, position);
            }
        });


        botaoDesistir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    public void pegarItens(View view, int posicao) {
        if (quantEscolhidos == 0) {
            preencherListaPrimeiroItem(view);
            primeiroItemEscolhido = listaItens.get(posicao);
            quantEscolhidos = 1;

        } else if (quantEscolhidos == 1){
            preencherListaSegundoItem(view);
            segundoItemEscolhido = listaItens.get(posicao);
            quantEscolhidos = 2;
            verificarSeItemCorreto(posicao);
        }
    }


    private void preencherListaPrimeiroItem(View view) {
        item1.add((ImageView) view.findViewById(R.id.imageView));
        item1.add((ImageView) view.findViewById(R.id.imageView2));
        item1.get(0).setVisibility(View.VISIBLE);
        view.setClickable(false);
        item1.get(1).setVisibility(View.INVISIBLE);
    }


    private void preencherListaSegundoItem(View view) {
        item2.add((ImageView) view.findViewById(R.id.imageView));
        item2.add((ImageView) view.findViewById(R.id.imageView2));
        item2.get(0).setVisibility(View.VISIBLE);
        view.setClickable(false);
        item2.get(1).setVisibility(View.INVISIBLE);
    }


    private void resetar() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                item1.get(0).setVisibility(View.INVISIBLE);
                item1.get(1).setVisibility(View.VISIBLE);
                item1.get(0).setEnabled(true);

                item2.get(0).setVisibility(View.INVISIBLE);
                item2.get(1).setVisibility(View.VISIBLE);
                item2.get(0).setEnabled(true);

                item1.clear();
                item2.clear();
                quantEscolhidos = 0;
            }
        }, 1000);

    }


    public void verificarSeItemCorreto(int position) {
        if (primeiroItemEscolhido.getImagem() == segundoItemEscolhido.getImagem()) {
            item1.clear();
            item2.clear();
            quantEscolhidos = 0;
            Toast.makeText(this, "Acertou mizeravii!", Toast.LENGTH_SHORT).show();
            acertos++;
            verificarSeGanhou();
        } else {
            Toast.makeText(this, "Errou mizeravii!", Toast.LENGTH_SHORT).show();
            resetar();
        }
    }


    private void verificarSeGanhou() {
        if (acertos >= 8) {
            Intent intent = new Intent(JogoActivity.this, ParabensActivity.class);
            startActivity(intent);
            finish();
        }
    }


}
